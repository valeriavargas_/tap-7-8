package practica1;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author valer
 */

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java .util.logging.Logger;
import javax.swing.DefaultListModel;

public class Hilos_Client {
    private Socket SocketClient;
    private DataInputStream entrada;
    private chat_Client client;
    private ObjectInputStream entradaObjeto;
    
    public Hilos_Client(Socket SocketClient, chat_Client client) { 
        this.SocketClient = SocketClient;
        this.client = client;
        
    }
    
    public void run() {
        while(true) {
            try {
                entrada =  new DataInputStream(SocketClient.getInputStream());
                client.messages(entrada.readUTF());
                
                entradaObjeto = new ObjectInputStream(SocketClient.getInputStream());
                client.listaConectados((DefaultListModel) entradaObjeto.readObject());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Hilos_Client.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Hilos_Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
