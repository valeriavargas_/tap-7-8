package practica1;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica1 implements ActionListener {
    JFrame ventana;
    JTextField nombre;
    JLabel espacioTexto;
    JButton btnSaludar;
    
    public static void main(String[] args) {
        Practica1 app = new Practica1();
        app.run();               
    }
    
    void run(){
        ventana = new JFrame("Proyecto 1");
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ventana.setLayout(new FlowLayout());
        ventana.setSize(400, 300);
        
        espacioTexto = new JLabel("Escribe un nombre para saludar");
        nombre = new JTextField(30);
        btnSaludar = new JButton("Saludar");
        btnSaludar.addActionListener(this);
        
        ventana.add(espacioTexto);
        ventana.add(nombre);
        ventana.add(btnSaludar);
        
        ventana.setVisible(true);        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(ventana, "Hola "+this.nombre.getText()+", saludos!");
    }
}


