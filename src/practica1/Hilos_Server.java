package practica1;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java .util.logging.Logger;
import javax.swing.DefaultListModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author valer
 */

public class Hilos_Server extends Thread {
    
    private DataInputStream entrada;
    private DataOutputStream salida;
    private chat_Server server;
    private Socket Client;
    public static Vector<Hilos_Server> usuarioActivo = new Vector();
    private String nombre;
    private ObjectOutputStream salidaObjeto;
    
    public Hilos_Server (Socket socketclient, String nombre, chat_Server serv) throws Exception {
        this.Client = socketclient;
        this.server = serv;
        this.nombre = nombre;
        usuarioActivo.add(this);
        
        for (int i = 0 ; i < usuarioActivo.size(); i++) {
            usuarioActivo.get(i).enviosMensajes(nombre + " se ha conectado.");
        }
    }
    
    public void run() {
        String mensaje = " ";
        while (true){
            try {
                entrada = new DataInputStream(Client.getInputStream());
                mensaje = entrada.readUTF();
                
                for (int i = 0; i < usuarioActivo.size(); i++) {
                    usuarioActivo.get(i).enviosMensajes(mensaje);
                    server.messages("Mensaje enviado");
                }
                
            } catch (Exception e) {
                break;
            }
        }
        
        usuarioActivo.removeElement(this);
        server.messages("El usuario se ha desconectado");
        
        try{
            Client.close();
        } catch (Exception e){
            
        }
    }
    
    private void enviosMensajes(String msg) throws Exception{
        salida = new DataOutputStream(Client.getOutputStream());
        salida.writeUTF(msg);
        DefaultListModel modelo = new DefaultListModel();
        
        for (int i = 0; i < usuarioActivo.size(); i++) {
            modelo.addElement(usuarioActivo.get(i).nombre);
        }
        salidaObjeto =  new ObjectOutputStream(Client.getOutputStream());
        salidaObjeto.writeObject(modelo);
    }
    
    
}

